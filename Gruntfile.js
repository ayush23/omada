'use strict';
module.exports = function (grunt) {
    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);
    // Show grunt task time
    require('time-grunt')(grunt);
    // Configurable paths for the app
    var appConfig = {
        app: 'app',
        dist: 'dist'
    };
    // Grunt configuration
    grunt.initConfig({

        // Project settings
        homer: appConfig,

        ngconstant: {
            options: {
                space: '  ',
                wrap: '"use strict";\n\n {\%= __ngModule %}',
                name: 'EnvironmentConfig'
            },

            development: {
                options: {
                    dest: '<%= homer.app %>/scripts/environment-config.js'
                },
                constants: {
                    // ENV: grunt.file.readJSON('app/config/dev-config.json'),
                    // BUILD_VARIABLES: {
                    //     BUILD_NO: 387,
                    //     BUILD_DATE: (new Date()).getTime(),
                    //     BUILD_ENV: 'Dev'
                    // }
                }
            },
            testing: {
                options: {
                    dest: '<%= homer.app %>/scripts/environment-config.js'
                },
                constants: {
                    // ENV: grunt.file.readJSON('app/config/test-config.json'),
                    // BUILD_VARIABLES: {
                    //     BUILD_NO: 297,
                    //     BUILD_DATE: (new Date()).getTime(),
                    //     BUILD_ENV: 'Test'
                    // }
                }
            },
            production: {
                options: {
                    dest: '<%= homer.app %>/scripts/environment-config.js'
                },
                constants: {
                    // ENV: grunt.file.readJSON('app/config/test-config.json')
                }
            }
        },


        // The grunt server settings
        connect: {
            options: {
                port: 9000,
                hostname: 'localhost',
                livereload: true
            },
            livereload: {
                options: {
                    open: true,
                    middleware: function (connect) {
                        return [
                            connect.static('.tmp'),
                            connect().use(
                                '/bower_components',
                                connect.static('./bower_components')
                            ),
                            connect.static(appConfig.app)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    open: true,
                    base: '<%= homer.dist %>'
                }
            }
        },
        // Compile less to css
        less: {
            development: {
                options: {
                    compress: true,
                    optimization: 2
                },
                files: {
                    "app/styles/style.css": "app/less/style.less"
                }
            }
        },
        // Watch for changes in live edit
        watch: {
            styles: {
                files: ['app/styles/**/*.css'],
                tasks: ['copy:styles_unminified'],
                options: {
                    nospawn: true,
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            scripts: {
                files: ['<%= homer.app %>/scripts/**/*.js'],
                tasks: ['copy:scripts'],
                options: {
                    nospawn: true,
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            modules: {
                files: ['<%= homer.app %>/modules/**/*.js', '<%= homer.app %>/modules/**/*.html'],
                tasks: ['copy:modules'],
                options: {
                    nospawn: true,
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            html: {
                files: ['<%= homer.app %>/*.html'],
                tasks: ['copy:dist'],
                options: {
                    nospawn: true,
                    livereload: '<%= connect.options.livereload %>'
                }
            },

            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= homer.app %>/**/*.html',
                    '<%= homer.app %>/**/*.js',
                    '.tmp/styles/{,*/}*.css',
                    '<%= homer.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },
        uglify: {
            options: {
                mangle: false
            }
        },
        // Clean dist folder
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= homer.dist %>/{,*/}*',
                        '!<%= homer.dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },
        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= homer.app %>',
                        dest: '<%= homer.dist %>',
                        src: [
                            '*.{ico,png,txt}',
                            '.htaccess',
                            '*.html',
                            'views/{,*/}*.html',
                            'styles/img/*.*',
                            'images/{,*/}*.*'
                        ]
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'bower_components/fontawesome',
                        src: ['fonts/*.*'],
                        dest: '<%= homer.dist %>'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'bower_components/bootstrap',
                        src: ['fonts/*.*'],
                        dest: '<%= homer.dist %>'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'app/fonts/pe-icon-7-stroke/',
                        src: ['fonts/*.*'],
                        dest: '<%= homer.dist %>'
                    }
                ]
            },
            styles: {
                expand: true,
                cwd: '<%= homer.app %>/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            },
            styles_unminified: {
                expand: true,
                cwd: '<%= homer.app %>/styles',
                dest: '<%= homer.dist %>/styles/',
                src: '**/*'
            },
            bower: {
                expand: true,
                dot: true,
                cwd: 'bower_components',
                dest: '<%= homer.dist %>/bower_components',
                src: '**/*'
            },
            fonts: {
                expand: true,
                dot: true,
                cwd: '<%= homer.app %>/fonts',
                dest: '<%= homer.dist %>/fonts',
                src: '**/*'
            },
            modules: {
                expand: true,
                dot: true,
                cwd: '<%= homer.app %>/modules',
                dest: '<%= homer.dist %>/modules',
                src: '**/*'
            },
            scripts: {
                expand: true,
                dot: true,
                cwd: '<%= homer.app %>/scripts',
                dest: '<%= homer.dist %>/scripts',
                src: '**/*'
            }
        },
        // Renames files for browser caching purposes
        filerev: {
            dist: {
                src: [
                    '<%= homer.dist %>/scripts/{,*/}*.js',
                    '<%= homer.dist %>/styles/{,*/}*.css',
                    '<%= homer.dist %>/styles/fonts/*'
                ]
            }
        },
        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= homer.dist %>',
                    src: ['*.html', 'views/{,*/}*.html'],
                    dest: '<%= homer.dist %>'
                }]
            }
        },
        useminPrepare: {
            html: 'app/index.html',
            options: {
                dest: 'dist'
            }
        },
        usemin: {
            html: ['dist/index.html']
        },
        // Append a timestamp to 'all js' & 'all css' which are both located in 'index.html'
        cachebreaker: {
            dev: {
                options: {
                    match: ['/*.js','/*.css'],
                    replacement: function () {
                        return new Date().getTime();
                    }
                },
                files: {
                    src: ['app/index.html','dist/index.html']
                }
            }
        }
    });

    grunt.registerTask('live', [
        'clean:server',
        'copy:styles',
        'connect:livereload',
        'watch'
    ]);
    grunt.registerTask('server', [
        'build',
        'connect:dist:keepalive'
    ]);
    grunt.registerTask('build', [
        'clean:dist',
        'less',
        'useminPrepare',
        'concat',
        'copy:dist',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        'htmlmin'
    ]);
    grunt.registerTask('buildDebug', [
        'clean:dist',
        'ngconstant:development',
        'copy:dist',
        'copy:styles_unminified',
        'copy:scripts',
        'copy:modules',
        'copy:fonts',
        'copy:bower',
        'connect:livereload',
        'watch'
    ]);
    grunt.registerTask('buildDev', [
        'clean:dist',
        'ngconstant:development',
        'copy:dist',
        'copy:styles_unminified',
        'copy:scripts',
        'copy:modules',
        'copy:fonts',
        'copy:bower',
        'cachebreaker'
    ]);

    grunt.registerTask('buildTesting', [
        'clean:dist',
        'ngconstant:testing',
        'copy:dist',
        'copy:styles_unminified',
        'copy:scripts',
        'copy:modules',
        'copy:fonts',
        'copy:bower',
        'cachebreaker'
    ]);

    grunt.registerTask('buildProd', [
        'clean:dist',
        'ngconstant:production',
        'copy:dist',
        'copy:styles_unminified',
        'copy:scripts',
        'copy:modules',
        'copy:fonts',
        'copy:bower',
        'cachebreaker'
    ]);
    grunt.registerTask('dev', [
        'connect:livereload',
        'watch'
    ]);
    grunt.loadNpmTasks('grunt-cache-breaker');
};