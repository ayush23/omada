/**
 * Created by manishg on 07/09/17.
 */
angular.module('homer')
    .controller('LoginController', ['$scope', '$rootScope', '$state', 'UserModel', 'Constants', 'Utility', 'SessionManager', 'LoginManager', 'Request', function ($scope, $rootScope, $state, UserModel, Constants, Utility, SessionManager, LoginManager, Request) {
        $scope.signIn = function () {
            document.getElementById('errorContainer').innerHTML = "";
            var requestObj = {
                pushToken: Utility.generateGUID()
            };
            var deviceModel = new Request(requestObj);

            LoginManager.registerDevice(deviceModel).then(function deviceResponse(response) {
                if (response && response.responseData && response.responseData._id) {
                    $scope.deviceID = response.responseData._id;
                    signInRequest();
                    SessionManager.storeDataInCookies($scope.deviceID, Constants.COOKIES.DEVICE_ID);
                }
                else {
                    document.getElementById('errorContainer').innerHTML = response.message[0].msg || 'Error';
                }
            });
        };

        $scope.signUp = function () {
            $state.go(Constants.STATE_ROUTING.SIGN_UP);
        };

        var signInRequest = function () {
            storeAndRemoveFromCookies();
            loginRequest();
        };

        var loginRequest = function () {
            //create a Request Object
            var requestObject = {
                emailID: $scope.userObject.emailID,
                password: $scope.userObject.password,
                deviceID: $scope.deviceID
            };
            var loginRequest = new Request(requestObject);

            LoginManager.login(loginRequest).then(function loginResponse(userResponse) {
                if (userResponse.userDetail && userResponse.userDetail.userID) {
                    SessionManager.storeDataInCookies(userResponse.userDetail, Constants.COOKIES.USER);
                    SessionManager.storeDataInCookies(userResponse.sessionToken, Constants.COOKIES.SESSION_TOKEN);
                    navigateToUserDashboard();
                    Utility.displayToast(userResponse.message[0].msg || Constants.MESSAGE.SUCCESS_MESSAGE.LOGIN_SUCCESSFULLY, Constants.TOAST_TYPE.SUCCESS);
                }
                else {
                    document.getElementById('errorContainer').innerHTML = userResponse.message[0].msg || Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR;
                }
            });
        };

        var storeAndRemoveFromCookies = function () {
            if ($scope.userObject.isRememberMe) {
                SessionManager.storeDataInCookies($scope.userObject.emailID, Constants.COOKIES.USER_EMAIL);
                SessionManager.storeDataInCookies($scope.userObject.password, Constants.COOKIES.USER_PASSWORD);
            }
            else {
                SessionManager.removeDataFromCookies(Constants.COOKIES.USER_EMAIL);
                SessionManager.removeDataFromCookies(Constants.COOKIES.USER_PASSWORD);
            }
            SessionManager.storeDataInCookies($scope.userObject.isRememberMe, Constants.COOKIES.REMEMBER_ME);
        };

        $scope.inItLoginFormParsley = function () {
            $('#loginForm').parsley();
        };

        var checkRememberMeFromCookies = function () {
            $scope.userObject.isRememberMe = SessionManager.getDataFromCookies(Constants.COOKIES.REMEMBER_ME);
            if ($scope.userObject.isRememberMe) {
                $scope.userObject.emailID = SessionManager.getDataFromCookies(Constants.COOKIES.USER_EMAIL);
                $scope.userObject.password = SessionManager.getDataFromCookies(Constants.COOKIES.USER_PASSWORD);
            }
        };

        var navigateToUserDashboard = function () {
            $state.go(Constants.STATE_ROUTING.PRIVATE_DASHBOARD);
        };

        var init = function () {
            $scope.userObject = new UserModel();
            checkRememberMeFromCookies();
        };

        if ($rootScope.validateSession()) {
            navigateToUserDashboard();
        }
        else {
            init();
        }

    }]);
