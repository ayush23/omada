angular
    .module('homer')
    .factory('LoginManager', ['$http', '$q', 'UserModel', 'HTTPService', 'SessionManager', 'Constants', 'Utility', function ($http, $q, UserModel, HTTPService, SessionManager, Constants, Utility) {

        var loginManager = {

            _pool: {},

            _retrieveUserInstance: function (userID, userData) {
                var userInstance = new UserModel(userData.userDetails);
                return userInstance;
            },

            login: function (requestModel) {
                var deferred = $q.defer();
                var scope = this;
                var url = Constants.WEBSERVICE.USER_SERVICE_URL + Constants.WEBSERVICE.LOG_IN;
                var data = {};

                data.emailID = requestModel.request.emailID;
                data.password = requestModel.request.password;
                data.deviceID = requestModel.request.deviceID;
                HTTPService.executeHttpPostRequest(url, data).then(function (loginResponse) {
                    var loginRequestResponse = {
                        userDetail: {},
                        sessionToken: '',
                        message: []
                    };
                    if (loginResponse && loginResponse.success && loginResponse.responseData) {
                        loginRequestResponse.userDetail = scope._retrieveUserInstance(loginResponse.responseData.userDetails._id, loginResponse.responseData);
                        loginRequestResponse.sessionToken = loginResponse.responseData.sessionToken;
                        loginRequestResponse.message = loginResponse.message;
                    }
                    else {
                        loginRequestResponse = loginResponse;
                    }

                    deferred.resolve(loginRequestResponse);
                });
                return deferred.promise;
            },

            logout: function (userID) {
                var deferred = $q.defer();
                var scope = this;
                var url = Constants.WEBSERVICE.USER_SERVICE_URL + Constants.WEBSERVICE.LOG_OUT;
                var data = {};
                data.user = userID;
                data.deviceID = SessionManager.getDataFromCookies(Constants.COOKIES.DEVICE_ID);
                HTTPService.executeHttpPostRequest(url, data).then(function (logoutResponse) {
                    deferred.resolve(logoutResponse);
                });
                return deferred.promise;
            },

            registerDevice: function (requestObj) {
                var deferred = $q.defer();
                var scope = this;
                var url = Constants.WEBSERVICE.USER_SERVICE_URL + Constants.WEBSERVICE.REGISTER_DEVICE;

                var data = {};
                data.deviceType = Constants.DEVICE_TYPE;
                data.pushToken = requestObj.request.pushToken;
                HTTPService.executeHttpPostRequest(url, data).then(function (response) {
                    var deviceModel = {};
                    if (!response || !response.responseData || !response.success) {
                        deviceModel = response;
                        Utility.displayToast(Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR, Constants.TOAST_TYPE.ERROR);
                    }
                    else {
                        deviceModel = response;
                    }
                    deferred.resolve(deviceModel);
                });

                return deferred.promise;
            }
        };

        return loginManager;
    }]);