angular
    .module('homer')
    .factory('SignUpManager', ['$http', '$q', 'UserModel', 'HTTPService', 'SessionManager', 'Constants', 'Utility', function ($http, $q, UserModel, HTTPService, SessionManager, Constants, Utility) {

        return {
            _pool: {},

            _retrieveUserInstance: function (userID, userData) {
                var userInstance = new UserModel(userData.userDetails);
                return userInstance;
            },

            signUp: function (requestModel) {
                var deferred = $q.defer();
                var scope = this;
                var url = Constants.WEBSERVICE.USER_SERVICE_URL + Constants.WEBSERVICE.SIGN_UP;

                var data = {};

                data.firstName = requestModel.request.firstName;
                data.lastName = requestModel.request.lastName;
                data.emailID = requestModel.request.emailID;
                data.password = requestModel.request.password;
                data.deviceID = requestModel.request.deviceID;

                HTTPService.executeHttpPostRequest(url, data).then(function (signUpResponse) {
                    var signUpRequestResponse = {
                        userDetail: {},
                        sessionToken: '',
                        message: []
                    };
                    if (signUpResponse && signUpResponse.success && signUpResponse.responseData) {
                        signUpRequestResponse.userDetail = scope._retrieveUserInstance(signUpResponse.responseData.userDetails._id, signUpResponse.responseData);
                        signUpRequestResponse.sessionToken = signUpResponse.responseData.sessionToken;
                        signUpRequestResponse.message = signUpResponse.message;
                    }
                    else {
                        signUpRequestResponse = signUpResponse;
                    }
                    deferred.resolve(signUpRequestResponse);
                });
                return deferred.promise;
            },
            registerDevice: function (requestObj) {
                var deferred = $q.defer();
                var scope = this;
                var url = Constants.WEBSERVICE.USER_SERVICE_URL + Constants.WEBSERVICE.REGISTER_DEVICE;

                var data = {};
                data.deviceType = Constants.DEVICE_TYPE;
                data.pushToken = requestObj.request.pushToken;
                HTTPService.executeHttpPostRequest(url, data).then(function (response) {
                    var deviceModel = {};
                    if (!response || !response.responseData || !response.success) {
                        deviceModel = response;
                        Utility.displayToast(Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR, Constants.TOAST_TYPE.ERROR);
                    }
                    else {
                        deviceModel = response;
                    }
                    deferred.resolve(deviceModel);
                });

                return deferred.promise;
            }
        }
    }]);