angular
    .module('homer')
    .factory('DashboardManager', ['$http', '$q', 'EntityModel', 'HTTPService', 'SessionManager', 'Constants', 'Utility', function ($http, $q, EntityModel, HTTPService, SessionManager, Constants, Utility) {

        return {

            _pool: {},

            _retrieveEntityInstance: function (entityID, entityObj) {
                return new EntityModel(entityObj);
            },

            getEntityList: function (requestData) {
                var deferred = $q.defer();
                var scope = this;

                var url = Constants.WEBSERVICE.ENTITY_SERVICE_URL + Constants.WEBSERVICE.GET_ENTITY_LIST + "?isActive=1";

                HTTPService.executeHttpGetRequest(url).then(function (getEntityListResponse) {
                    var parsedGetEntityListResponse = {};
                    if (getEntityListResponse && getEntityListResponse.success && getEntityListResponse.responseData) {

                        if (getEntityListResponse.responseData.entityList) {
                            var parsedEntityList = [];
                            var entityList = getEntityListResponse.responseData.entityList;

                            for (var entityIndex = 0; entityIndex < entityList.length; entityIndex++) {
                                parsedEntityList.push(scope._retrieveEntityInstance(entityList[entityIndex]._id, entityList[entityIndex]));
                            }

                            parsedGetEntityListResponse['entityList'] = parsedEntityList;
                        } else {
                            parsedGetEntityListResponse['entityList'] = [];
                        }

                        if (getEntityListResponse.responseData.Token) {
                            parsedGetEntityListResponse['token'] = getEntityListResponse.responseData.Token;
                        } else {
                            parsedGetEntityListResponse['token'] = {};
                        }

                    } else {
                        parsedGetEntityListResponse['entityList'] = [];
                        parsedGetEntityListResponse['token'] = {};
                    }

                    parsedGetEntityListResponse['message'] = getEntityListResponse.message;

                    deferred.resolve(parsedGetEntityListResponse);
                });

                return deferred.promise;
            }
        };
    }]);