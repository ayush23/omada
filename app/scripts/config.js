/**
 * HOMER - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 */

function configState($stateProvider, $urlRouterProvider, $compileProvider, Constants) {
    // Optimize load start with remove binding information inside the DOM element
    $compileProvider.debugInfoEnabled(true);

    // Set default state
    $urlRouterProvider.otherwise("/login");
    $stateProvider
    // Login
        .state(Constants.STATE_ROUTING.LOGIN, {
            url: "/login",
            templateUrl: "modules/login/login.html",
            controller: 'LoginController',
            data: {
                pageTitle: 'LoginIn'
            }
        })
        // Dashboard - Main page
        .state(Constants.STATE_ROUTING.MAIN_DASHBOARD, {
            url: "/dashboard",
            templateUrl: "modules/dashboard/dashboard.html",
            controller: 'DashboardController',
            data: {
                pageTitle: 'Dashboard'
            }
        })
        // Dashboard - Main page
        .state(Constants.STATE_ROUTING.SIGN_UP, {
            url: "/sign-up",
            templateUrl: "modules/signUp/signUp.html",
            controller: 'SignUpController',
            data: {
                pageTitle: 'SignUp'
            }
        })
        .state(Constants.STATE_ROUTING.PRIVATE_DASHBOARD, {
            url: "/private-dashboard",
            templateUrl: "modules/private-dashboard/private-dashboard.html",
            controller: 'PrivateDashboardController',
            data: {
                pageTitle: 'Private Dashboard'
            }
        })
}

angular
    .module('homer')
    .config(configState)
    .run(function ($rootScope, $state) {
        $rootScope.$state = $state;
    });