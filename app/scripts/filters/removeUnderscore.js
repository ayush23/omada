/**
 * Created by manishg on 21/10/16.
 */
angular
    .module('homer')
    .filter('removeUnderscore', removeUnderscore);

function removeUnderscore() {
    return function (input) {
        if (angular.isUndefined(input) || !angular.isString(input) || (input.length <= 0))
            return " ";
        else {
            return input.replace(/_/g, " ");
        }
    };
}