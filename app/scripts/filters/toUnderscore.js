/**
 * Created by manishg on 7/4/2017.
 */
angular
    .module('homer')
    .filter('toUnderscore', toUnderscore);

function toUnderscore() {
    return function (input) {
        if (!input)
            return "";
        else
            return input.replace(/ /g, "_");
    };
}