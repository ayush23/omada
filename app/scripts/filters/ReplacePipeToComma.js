/**
 * Created by manishg on 03/10/16.
 */
/**
 * Created by manishg on 03/10/16.
 */

angular
    .module('homer')
    .filter('replacePipeToComma', replacePipeToComma);

function replacePipeToComma() {
    return function (input) {
        if (angular.isUndefined(input) || !angular.isString(input) || (input.length <= 0))
            return " ";
        else {
            return input.replace(/\|\|/g, ",");
        }
    };
}