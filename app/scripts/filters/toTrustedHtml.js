/**
 * Created by manishg on 1/19/2017.
 */
angular
    .module('homer')
    .filter('toTrusted', ['$sce', function ($sce) {
        return function (text) {
            return $sce.trustAsHtml(text);
        };
    }]);