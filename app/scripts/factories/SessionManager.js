angular
    .module("homer").factory('SessionManager', ['$rootScope', '$cookies', 'Constants', '$window', function ($rootScope, $cookies, Constants, $window) {

    function storeDataInCookies(key, value) {
        if (value === null)
            return;
        var now = new Date();
        var expiryDate = new Date(now.getFullYear() + 1, now.getMonth(), now.getDate()); // expires after a year
        $cookies.putObject(key, value, {expires: expiryDate});
    }

    function getDataFromCookies(key) {
        return $cookies.getObject(key);
    }

    function removeDataFromCookies(key) {
        $cookies.remove(key);
    }

    function storeDataInSession(key, value) {
        if (value === null)
            return;
        $window.sessionStorage.setItem(key, angular.toJson(value));
    }

    function getDataFromSession(key) {
        return angular.fromJson($window.sessionStorage.getItem(key));
    }

    function storeStringDataInLocalStorage(key, value) {
        if (value === null || angular.isUndefined(value))
            return;
        $window.localStorage.setItem(key, value);
    }

    function getStringDataFromLocalStorage(key) {
        return $window.localStorage.getItem(key);
    }

    function removeDataInLocalStorage(key) {
        $window.localStorage.removeItem(key);
    }

    var sessionManager = {
        storeStringDataInLocalStorage: function (key, value) {
            if (key === undefined || key === null || value === undefined || value === null)
                return;
            storeStringDataInLocalStorage(key, value);
        },

        getStringDataFromLocalStorage: function (key) {
            var value = getStringDataFromLocalStorage(key);
            if ((angular.isUndefined(value) || value === null))
                return false;
            else {
                return value;
            }
        },

        removeStringDataFromLocalStorage: function (key) {
            if (key === undefined || key === null)
                return;
            return removeDataInLocalStorage(key)
        },

        storeDataInCookies: function (modelObj, key) {
            if (modelObj === null)
                return;
            storeDataInCookies(key, modelObj);
        },

        getDataFromCookies: function (key) {
            var modelObj = getDataFromCookies(key);
            if ((angular.isUndefined(modelObj) || modelObj === null))
                return false;
            else
                return modelObj;
        },

        storeDataInSession: function (modelObj, key) {
            if (modelObj === null)
                return;
            storeDataInSession(key, modelObj);
        },

        getDataFromSession: function (key) {
            var modelObj = getDataFromSession(key);
            if ((angular.isUndefined(modelObj) || modelObj === null))
                return false;
            else
                return modelObj;
        },

        isUserLoggedIn: function () {
            var userModel = getDataFromCookies(Constants.COOKIES.USER);
            if (!angular.isUndefined(userModel) && (userModel !== null) && (userModel.hasOwnProperty('_id') && (userModel.hasOwnProperty('userID')))) {
                return !((angular.isUndefined(userModel._id)) || (userModel._id === null) || (userModel._id === "") || (angular.isUndefined(userModel.userID)) || (userModel.userID === null) || (userModel.userID === ""));
            }
            return false;
        },
        removeDataFromCookies: function (key) {
            removeDataFromCookies(key);
        },
        removeAllCookies: function () {
            var allCookies = $cookies.getAll();
            var keys = Object.keys(allCookies);
            angular.forEach(keys, function (key) {
                if (key !== Constants.COOKIES.REMEMBER_ME && key !== Constants.COOKIES.USER_EMAIL && key !== Constants.COOKIES.USER_PASSWORD)
                    removeDataFromCookies(key);
            });
            $window.localStorage.clear();
        }
    };
    return sessionManager;
}]);