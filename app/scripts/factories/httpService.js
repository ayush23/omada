angular
    .module("homer")
    .factory("HTTPService", ["$http", "$q", "$location", function ($http, $q, $location) {

        return {
            executeHttpGetRequest: function (url) {
                // var userData = sessionManager.getUserDataFromCookies();
                var deferred = $q.defer();

                $http.defaults.headers.put = {
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
                    'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
                    'Access-Control-Allow-Origin': '*'

                };
                $http.get(url).then(function (response) {
                    if ((!response.data.success) && (response.data.responseCode == 401)) {
                        $location.path("/login#se");
                    }
                    deferred.resolve(response.data);
                }).catch(function (response) {
                    deferred.reject(response);
                });

                return deferred.promise;

            },
            executeHttpPostRequest: function (url, data, headers) {
                var deferred = $q.defer();
                $http.defaults.headers.put = {
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
                    'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
                    'Access-Control-Allow-Origin': '*'

                };
                var paramData = {};
                if (!headers) {
                    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                    paramData = $.param(data); // To convert json into post params
                }
                else {
                    $http.defaults.headers.post["Content-Type"] = headers;
                    paramData = data; // To convert json into post params
                }

                $http.post(url, paramData).then(function (response) {
                    if ((!response.data.success) && (response.data.message[0].responseCode === 401)) {
                        $location.path("/login");
                    }
                    deferred.resolve(response.data);
                }).catch(function (response) {
                    console.log(response);
                    deferred.reject();
                });
                return deferred.promise;
            },
            executeHttpPutRequest: function (url, data, headers) {

                var deferred = $q.defer();
                var paramData = {};

                if (!headers) {
                    $http.defaults.headers.put["Content-Type"] = "application/x-www-form-urlencoded";
                    paramData = $.param(data);
                }
                else {
                    $http.defaults.headers.put["Content-Type"] = headers;
                    paramData = data;
                }

                $http.put(url, paramData).then(function (response) {
                    deferred.resolve(response.data);
                }).catch(function (response) {
                    console.log(response);
                    deferred.reject();
                });
                return deferred.promise;
            },
            executeHttpDeleteRequest: function (url, data) {
                var deferred = $q.defer();
                if (data)
                    var paramData = $.param(data); // To convert json into post params


                $http.delete(url, paramData).then(function (response) {
                    deferred.resolve(response);
                }).catch(function (response) {
                    console.log(response);
                    deferred.reject();
                });
                return deferred.promise;
            }

        }
    }]);

