angular.module('homer').constant('Constants', {

    WEBSERVICE: {
        USER_SERVICE_URL: 'http://leewayhertz.biz:3002/',
        ENTITY_SERVICE_URL: 'http://leewayhertz.biz:3001/',
        LOG_IN: 'login',
        SIGN_UP: 'register-user',
        LOG_OUT: 'logout',
        FORGOT_PASSWORD: 'forgot-password',
        REGISTER_DEVICE: 'register-device',
        GET_ENTITY_LIST: 'get-entity-list',
        ADD_ENTITY: 'add-entity',
        UPDATE_ENTITY: 'update-entity'
    },

    DEVICE_TYPE: 'web',
    ENTITY_TYPE: {PROBLEM: 'PROBLEM'},

    STATE_ROUTING: {
        LOGIN: 'login',
        MAIN_DASHBOARD: 'mainDashboard',
        SIGN_UP: 'signUp',
        PRIVATE_DASHBOARD: 'privateDashboard'
    },
    COOKIES: {
        USER: 'user',
        USER_ROLE: 'userRole',
        DEVICE_ID: 'deviceID',
        SESSION_TOKEN: 'sessionToken',
        ISLAND_LIST: 'islandList',
        SELECTED_ISLAND: 'selectedIsland',
        REMEMBER_ME: 'rememberMe',
        USER_EMAIL: 'userEmail',
        USER_PASSWORD: 'userPassword'
    },
    NOTIFICATION: {
        'REGISTER_DEVICE_NOTIFICATION': 'registerDeviceNotification',
        'LOGIN_NOTIFICATION': 'loginNotification'
    },
    MESSAGE: {
        ERROR_MESSAGE: {
            WEBSERVICE_RESPONSE_ERROR: 'Some error has occurred while handling your request. Please try again later!!',
            INVALID_SESSION: 'Your Session is expired.'
        },
        SUCCESS_MESSAGE: {
            LOGIN_SUCCESSFULLY: 'Login successfully',
            LOGOUT_SUCCESSFULLY: 'Logout successfully',
            WEBSERVICE_RESPONSE_SUCCESS: 'Success!!',
            SUBMIT_PROBLEM_SUCCESS: 'Your Query has added Successfully',
            SUBMIT_SOLUTION_SUCCESS: 'Your solution has added Successfully',
            SOLVED_PROBLEM_SUCCESS: 'Your Query has marked as solved'
        }
    },
    TOAST_DISPLAY_TIME: {
        MS_1500: '1500',
        MS_2000: '2000',
        MS_2500: '2500',
        MS_3000: '3000'
    },

    TOAST_TYPE: {
        INFO: 'INFO',
        SUCCESS: 'SUCCESS',
        WARNING: 'WARNING',
        ERROR: 'ERROR'
    }
});

