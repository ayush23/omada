/**
 * Created by manishg on 22/02/17.
 */
angular.module('homer').factory('EntityModel', function () {

    function EntityModel(entity) {
        this._id = "";
        this.entityType = 'PROBLEM';
        this.entityData = {
            description: "",
            postedBy: {userID: "", firstName: "", lastName: ""},
            solvedBy: {userID: "", firstName: "", lastName: ""},
            rewardDescription: "",
            solutions: []
        };

        this.addedOn = 0;
        this.modifiedOn = 0;
        this.isActive = 0;
        this.isDeleted = 0;
        if (entity) {
            this.setData(entity);
        }
    }

    EntityModel.prototype = {
        setData: function (entity) {
            this._id = entity._id ? entity._id : "";
            this.entityType = entity.entityType ? entity.entityType : 'PROBLEM';
            if (entity.entityData) {
                this.entityData.description = entity.entityData.description ? entity.entityData.description : "";
                this.entityData.postedBy = entity.entityData.postedBy ? entity.entityData.postedBy : {
                    userID: "",
                    firstName: "",
                    lastName: ""
                };
                this.entityData.solvedBy = entity.entityData.solvedBy ? entity.entityData.solvedBy : {
                    userID: "",
                    firstName: "",
                    lastName: ""
                };
                this.entityData.rewardDescription = entity.entityData.rewardDescription ? entity.entityData.rewardDescription : "";
                if (entity.entityData.solutions) {
                    var solutions = [];
                    var solutionModel = {};
                    angular.forEach(entity.entityData.solutions, function (solution) {
                        solutionModel = {};
                        solutionModel._id = solution._id ? solution._id : "";
                        solutionModel.description = solution.description ? solution.description : "";
                        solutionModel.postedBy = solution.postedBy ? solution.postedBy : {
                            userID: "",
                            firstName: "",
                            lastName: ""
                        };
                        solutionModel.votedBy = solution.votedBy ? solution.votedBy : [];
                        solutionModel.isSolution = solution.isSolution ? solution.isSolution : 0;
                        solutionModel.addedOn = solution.addedOn ? solution.addedOn : 0;
                        solutionModel.modifiedOn = solution.modifiedOn ? solution.modifiedOn : 0;
                        solutions.push(solutionModel);
                    });

                    this.entityData.solutions = solutions;
                }

            }
            this.addedOn = entity.addedOn ? entity.addedOn : 0;
            this.modifiedOn = entity.modifiedOn ? entity.modifiedOn : 0;
            this.isActive = entity.isActive ? entity.isActive : 0;
            this.isDeleted = entity.isDeleted ? entity.isDeleted : 0;
        }
    };

    return (EntityModel);
});