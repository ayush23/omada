/**
 * Created by manishg on 22/02/17.
 */
angular.module('homer').factory('UserModel', function () {

    function UserModel(user) {
        this._id = "";
        this.userID = "";
        this.firstName = "";
        this.lastName = "";
        this.fullName = "";
        this.role = [];
        this.emailID = "";
        this.phone = "";
        this.miscellaneous = "";
        this.photo = {
            'modifiedOn': 0,
            'addedOn': 0,
            'url': "",
            'fileName': ""
        };
        this.isInvite = false;
        this.addedOn = 0;
        this.modifiedOn = 0;
        this.isInactive = 0;
        this.isDeleted = 0;
        if (user) {
            this.setData(user);
        }
    }

    UserModel.prototype = {
        setData: function (user) {
            this._id = user._id ? user._id : "";
            this.userID = user.userID ? user.userID : "";
            this.firstName = user.firstName ? user.firstName : "";
            this.lastName = user.lastName ? user.lastName : "";
            this.fullName = this.firstName;
            if (this.lastName.length > 0)
                this.fullName += " " + this.lastName;
            this.role = user.role ? user.role : [];
            this.emailID = user.emailID ? user.emailID : "";
            this.phone = user.phone ? user.phone : "";
            this.miscellaneous = user.miscellaneous ? user.miscellaneous : "";
            this.photo = user.photo ? user.photo : {'modifiedOn': 0, 'addedOn': 0, 'url': "", 'fileName': ""};
            this.isInvite = user.isInvite ? user.isInvite : false;
            this.addedOn = user.addedOn ? user.addedOn : 0;
            this.modifiedOn = user.modifiedOn ? user.modifiedOn : 0;
            this.isInactive = user.isInactive ? user.isInactive : 0;
            this.isDeleted = user.isDeleted ? user.isDeleted : 0;
        }
    };
    return (UserModel);
});