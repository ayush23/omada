/**
 * HOMER - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 */
(function () {
    var app = angular.module('homer', [
        'ui.router',                // Angular flexible routing
        'ui.bootstrap',             // AngularJS native directives for Bootstrap
        'ngMaterial',
        'md.data.table',
        'ngCookies',
        'timer',
        'summernote',
        'ngSanitize'
    ]);
    app.run(['$rootScope', 'SessionManager', '$state', 'Constants', 'LoginManager', 'Utility', function ($rootScope, SessionManager, $state, Constants, LoginManager, Utility) {

        $rootScope.validateSession = function () {
            if (!SessionManager.isUserLoggedIn()) {
                SessionManager.removeAllCookies();
                $state.go(Constants.STATE_ROUTING.LOGIN);
                return false;
            }
            return true;
        };

        $rootScope.logOutUser = function () {
            var user = SessionManager.getDataFromCookies(Constants.COOKIES.USER);
            LoginManager.logout(user.userID).then(function (logoutResponse) {
                if (logoutResponse.success) {
                    Utility.displayToast(logoutResponse.message[0].msg || Constants.MESSAGE.SUCCESS_MESSAGE.LOGOUT_SUCCESSFULLY, Constants.TOAST_TYPE.SUCCESS);
                    SessionManager.removeAllCookies();
                    $state.go(Constants.STATE_ROUTING.LOGIN);
                } else {
                    Utility.displayToast(logoutResponse.message[0].msg || Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR, Constants.TOAST_TYPE.ERROR);
                }

            });
        };
    }])
})();

