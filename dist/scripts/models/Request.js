angular
    .module('homer')
    .factory('Request',['Constants', function(Constants){
        function Request(request){
            this.request = request;
        }
        return Request;

    }]);