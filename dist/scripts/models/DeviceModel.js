/**
 * Created by manishg on 22/02/17.
 */
angular.module('homer').factory('DeviceModel', function () {

    function DeviceModel(device) {
        this.lhtDeviceID = "";
        this.id = "";
        this.name = "";
        this.type = "";
        this.osType = "";
        this.os = "";
        this.availability = "";
        this.location = "";
        this.locationHistory = [];
        this.addedOn = 0;
        this.modifiedOn = 0;
        if (device) {
            this.setData(device);
        }
    }

    DeviceModel.prototype = {
        setData: function (device) {
            this.lhtDeviceID = device.lhtDeviceID ? device.lhtDeviceID : '';
            this.id = device.id ? device.id : "";
            this.name = device.name ? device.name : "";
            this.type = device.type ? device.type : "";
            this.osType = device.osType ? device.osType : "";
            this.os = device.os ? device.os : "";
            this.availability = device.availability ? device.availability : "";
            this.location = device.location ? device.location : "";
            this.locationHistory = device.locationHistory ? device.locationHistory : [];
            this.addedOn = device.addedOn ? device.addedOn : 0;
            this.modifiedOn = device.modifiedOn ? device.modifiedOn : 0;
        }
    };
    return (DeviceModel);
});