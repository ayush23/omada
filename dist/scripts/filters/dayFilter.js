/**
 * Created by manishg on 28/09/16.
 */
angular
    .module('homer')
    .filter('dayFilter', dayFilter);

function dayFilter() {
    return function (timeArray) {
        var timeValue = '';
        if ((timeArray != undefined) && (timeArray.length != 0)) {
            timeValue = timeArray.join(', ');
        } // this will make string an array
        return timeValue.replace(/\|\|/g, " - ");
    }
}