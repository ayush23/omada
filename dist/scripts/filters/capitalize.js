angular
    .module('homer')
    .filter('capitalize', capitalize);

function capitalize() {
    return function (input) {
        // return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        return (!!input) ? input.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }) : '';
    }
}