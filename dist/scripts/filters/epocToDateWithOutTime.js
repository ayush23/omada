/**
 * Created by manishg on 3/15/2017.
 */

angular
    .module('homer')
    .filter('epocToDateWithOutTime', epocToDateWithOutTime);

function epocToDateWithOutTime() {
    return function (seconds) {
        if (!seconds || seconds === "" || seconds === 0)
            return "";
        seconds = Math.floor(seconds);
        return moment(new Date(seconds)).format('DD MMMM YYYY');
    }
}