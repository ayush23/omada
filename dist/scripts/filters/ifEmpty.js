/**
 * Created by manishg on 4/24/2017.
 */
angular
    .module('homer')
    .filter('ifEmpty', function () {
        return function (input, defaultValue) {
            if (angular.isUndefined(input) || input === null || input === '') {
                return defaultValue || "--";
            }
            return input;
        }
    });