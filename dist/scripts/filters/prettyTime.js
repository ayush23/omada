/**
 * Created by Nagendra on 9/27/16.
 */

angular
    .module('homer')
    .filter('prettyTime', prettyTime);

function prettyTime() {
    return function (seconds) {
        seconds = Math.floor(seconds);//to convert to integer if seconds in String.
        var nowTimeMilliseconds = (new Date).getTime();
        var date = new Date(seconds);
        var dateObject = moment(date).format('DD MMMM YYYY');
        //var dateObject = moment(date).format('ddd, MMM DD hh:mm A');
        seconds = Math.floor((nowTimeMilliseconds / 1000) - (seconds / 1000));

        var interval = Math.floor(seconds / 172800);
        if (interval >= 1)
            return dateObject;
        //if (interval >= 1) return dateObject+" "+moment.tz(moment.tz.guess()).format('z');
        interval = Math.floor(seconds / 86400);
        if (interval >= 1)
            return "yesterday";

        interval = Math.floor(seconds / 3600);
        if (interval >= 1) {
            if (interval === 1)
                return interval + " hr ago";
            return interval + " hrs ago";
        }

        interval = Math.floor(seconds / 60);
        if (interval >= 1) {
            if (interval === 1)
                return interval + " minute ago";
            return interval + " minutes ago";
        }
        else
            return "0 minute ago";
    }
}