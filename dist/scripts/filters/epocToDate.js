/**
 * Created by manishg on 1/5/2017.
 */

angular
    .module('homer')
    .filter('epocToDate', epocToDate);

function epocToDate() {
    return function (seconds) {
        if (!seconds || seconds === "" || seconds === 0)
            return "";
        seconds = Math.floor(seconds);
        return moment(new Date(seconds)).format('DD MMMM YYYY HH:mm');
        // return moment(new Date(seconds)).format('hh:mm:ss a DD MMMM YYYY');
        // return new Date(seconds).toLocaleDateString();
    }
}