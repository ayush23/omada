/**
 * Created by manishg on 03/10/16.
 */

angular
    .module('homer')
    .filter('arrayJoin', arrayJoin);

function arrayJoin() {
    return function (input) {
        if ((input != undefined) && (input.length != 0)) {
            var ar = input.join(',');
        } // this will make string an array
        return ar;
    };
}