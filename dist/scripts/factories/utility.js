angular
    .module('homer')
    .factory('Utility', ['$mdToast', '$mdDialog', 'Constants', function ($mdToast, $mdDialog, Constants) {
        return {
            epochToPrettyDateTime: function (seconds) {
                if (!seconds || seconds === "" || seconds === 0)
                    return "";
                seconds = Math.floor(seconds);
                return moment(new Date(seconds)).format('DD MMMM YYYY HH:mm');
            },

            epochToDateWithOutTime: function (seconds) {
                if (!seconds || seconds === "" || seconds === 0)
                    return "";
                seconds = Math.floor(seconds);
                return moment(new Date(seconds)).format('DD MMMM YYYY');
            },

            epochToPrettyTime: function (seconds) {
                seconds = Math.floor(seconds);//to convert to integer if seconds is String.
                var nowTimeMilliseconds = (new Date).getTime();
                var date = new Date(seconds);
                var dateObject = moment(date).format('DD MMMM YYYY');
                //var dateObject = moment(date).format('ddd, MMM DD hh:mm A');
                seconds = Math.floor((nowTimeMilliseconds / 1000) - (seconds / 1000));
                var interval = Math.floor(seconds / 172800);
                if (interval >= 1)
                    return dateObject;
                //if (interval >= 1) return dateObject+" "+moment.tz(moment.tz.guess()).format('z');
                interval = Math.floor(seconds / 86400);
                if (interval >= 1)
                    return "yesterday";

                interval = Math.floor(seconds / 3600);
                if (interval >= 1) {
                    if (interval == 1)
                        return interval + " hr ago";
                    return interval + " hrs ago";
                }
                interval = Math.floor(seconds / 60);
                if (interval >= 1) {
                    if (interval == 1)
                        return interval + " minute ago";
                    return interval + " minutes ago";
                }
                else
                    return "0 minute ago";
            },
            displayToast: function (message, toastType, timeout) {
                toastr.options.timeOut = timeout || Constants.TOAST_DISPLAY_TIME.MS_1500;
                toastr.options.closeButton = true;
                toastr.options.tapToDismiss = true;

                if (toastType === Constants.TOAST_TYPE.SUCCESS)
                    toastr.success(message);
                else if (toastType === Constants.TOAST_TYPE.WARNING)
                    toastr.warning(message);
                else if (toastType === Constants.TOAST_TYPE.ERROR)
                    toastr.error(message);
                else
                    toastr.info(message);
            },

            appendWithEpochTime: function (name) {
                var epochTime = (new Date).getTime();
                return epochTime + name.replace(/ /g, '');
            },

            generateAppPrimaryKey: function () {
                var currentTimeStamp = (new Date().getTime()).toString();
                return currentTimeStamp + "_" + this.generateRandomAlphaNumericString(13) + "_CMS";

            },
            generateRandomAlphaNumericString: function (length) {
                var randomAlphaNumericString = "";
                var charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                for (var i = 0; i < length; i++)
                    randomAlphaNumericString += charset.charAt(Math.floor(Math.random() * charset.length));
                return randomAlphaNumericString;
            },
            generateGUID: function () {
                var nav = window.navigator;
                var screen = window.screen;
                var guid = nav.mimeTypes.length;
                guid += nav.userAgent.replace(/\D+/g, '');
                guid += nav.plugins.length;
                guid += screen.height || '';
                guid += screen.width || '';
                guid += screen.pixelDepth || '';
                return guid;
            },

            sweetAlert: function (message) {
                swal({
                    title: message,
                    text: "",
                    imageUrl: "images/version-logo.png"
                });
            },

            showConfirmDialogBox: function (message) {
                return $mdDialog.confirm()
                    .title(message)
                    .textContent('')
                    // .targetEvent(event)
                    .ok('Ok')
                    .cancel('Cancel');
            },

            showAlertDialogBox: function (message) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title(message)
                        //.textContent('You can specify some description text in here.')
                        .ok('Ok')
                );
            },

            convertDateStringToEpochTime: function (dateString) {
                if (!dateString || dateString === "")
                    return 0;
                return new Date(dateString).getTime();
            },
            replacePipeSymbolToComma: function (input) {
                if (angular.isUndefined(input) || !angular.isString(input) || (input.length <= 0))
                    return "";
                else {
                    return input.replace(/\|\|/g, ", ");
                }
            },
            getMiniUserModel: function (userObject) {
                if (!userObject)
                    return;
                var miniUserModel = {};
                miniUserModel.userID = userObject.userID;
                miniUserModel.firstName = userObject.firstName;
                miniUserModel.lastName = userObject.lastName;
                return miniUserModel;
            },
            getRandomInt: function (min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
        };
    }]);