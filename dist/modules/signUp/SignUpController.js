/**
 * Created by manishg on 07/09/17.
 */
angular.module('homer')
    .controller('SignUpController', ['$scope', '$rootScope', '$state', 'UserModel', 'Constants', 'Utility', 'SessionManager', 'SignUpManager', 'Request', function ($scope, $rootScope, $state, UserModel, Constants, Utility, SessionManager, SignUpManager, Request) {
        $scope.signUp = function () {
            document.getElementById('errorContainer').innerHTML = "";
            var requestObj = {
                pushToken: Utility.generateGUID()
            };
            var deviceModel = new Request(requestObj);

            SignUpManager.registerDevice(deviceModel).then(function deviceResponse(response) {
                if (response && response.responseData && response.responseData._id) {
                    $scope.deviceID = response.responseData._id;
                    signUpRequest();
                    SessionManager.storeDataInCookies($scope.deviceID, Constants.COOKIES.DEVICE_ID);
                }
                else {
                    document.getElementById('errorContainer').innerHTML = response.message[0].msg || 'Error';
                }
            });
        };

        var signUpRequest = function () {
            //create a Request Object
            var requestObject = {
                firstName: $scope.userObject.firstName,
                lastName: $scope.userObject.lastName,
                emailID: $scope.userObject.emailID,
                password: $scope.userObject.password,
                deviceID: $scope.deviceID
            };
            var signUpRequest = new Request(requestObject);

            SignUpManager.signUp(signUpRequest).then(function signUpResponse(userResponse) {
                if (userResponse.userDetail && userResponse.userDetail.userID) {
                    SessionManager.storeDataInCookies(userResponse.userDetail, Constants.COOKIES.USER);
                    SessionManager.storeDataInCookies(userResponse.sessionToken, Constants.COOKIES.SESSION_TOKEN);
                    navigateToUserDashboard();
                    Utility.displayToast(userResponse.message[0].msg || Constants.MESSAGE.SUCCESS_MESSAGE.LOGIN_SUCCESSFULLY, Constants.TOAST_TYPE.SUCCESS);
                }
                else {
                    document.getElementById('errorContainer').innerHTML = userResponse.message[0].msg || Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR;
                }
            });
        };

        $scope.inItSignUpFormParsley = function () {
            $('#SignUpForm').parsley();
        };

        var navigateToUserDashboard = function () {
            $state.go(Constants.STATE_ROUTING.PRIVATE_DASHBOARD);
        };

        var init = function () {
            if (SessionManager.isUserLoggedIn())
                navigateToUserDashboard();
            else
                SessionManager.removeAllCookies();
            $scope.userObject = new UserModel();
        };
        init();

    }]);
