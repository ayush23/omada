/**
 * Created by manishg on 07/09/17.
 */
angular.module('homer')
    .controller('DashboardController', ['$scope', '$rootScope', '$state', 'Constants', 'Utility', 'DashboardManager', '$timeout', function ($scope, $rootScope, $state, Constants, Utility, DashboardManager, $timeout) {
        var getProblemList = function () {
            var requestData = {};
            DashboardManager.getEntityList(requestData).then(function getEntityListResponse(getEntityListResponse) {
                if (getEntityListResponse && getEntityListResponse.entityList && getEntityListResponse.token) {
                    $scope.token = getEntityListResponse.token;
                    $scope.problemList = getEntityListResponse.entityList;
                    setSelectedProblem(0);
                } else {
                    Utility.displayToast(getEntityListResponse.message[0].msg || Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR, Constants.TOAST_TYPE.ERROR);
                }
            });
        };

        var setSelectedProblem = function (problemIndex) {
            if ($scope.problemList && $scope.problemList[problemIndex])
                $scope.selectedProblem = $scope.problemList[problemIndex];
        };

        var changeSelectedProblem = function () {
            setInterval(function () {
                if ($scope.problemList.length > 0) {
                    $scope.selectedProblemIndex++;
                    $scope.selectedProblemIndex = $scope.selectedProblemIndex % $scope.problemList.length;
                    setSelectedProblem($scope.selectedProblemIndex);
                }
            }, 20000);
        };

        $scope.getSelectedProblem = function (problem) {
            $scope.selectedProblem = problem;
        };

        $scope.isSolutionSolvedProblem = function (solution) {
            return solution.isSolution;
        };

        $scope.carousel = function () {
            // var i;
            // var x = document.getElementsByClassName("mySlides");
            // for (i = 0; i < x.length; i++) {
            //     x[i].style.display = "none";
            // }
            // myIndex++;
            // if (myIndex > x.length) {
            //     myIndex = 1
            // }
            // x[myIndex - 1].style.display = "block";
            // setTimeout(carousel, 2000); // Change image every 2 seconds
        };

        var init = function () {
            $scope.myInterval = 1000;
            $scope.selectedProblem = {};
            $scope.problemList = [];
            getProblemList();
            setInterval(function () {
                getProblemList();
            }, 60000);
            $scope.selectedProblemIndex = 0;
            changeSelectedProblem();
            $scope.myIndex = 0;
            // $scope.carousel();
        };

        init();
    }]);
