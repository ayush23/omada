angular
    .module('homer')
    .factory('PrivateDashboardManager', ['$http', '$q', 'EntityModel', 'HTTPService', 'SessionManager', 'Constants', 'Utility', function ($http, $q, EntityModel, HTTPService, SessionManager, Constants, Utility) {

        return {

            _pool: {},

            _retrieveEntityInstance: function (entityID, entityObj) {
                return new EntityModel(entityObj);
            },

            getEntityList: function (requestData) {
                var deferred = $q.defer();
                var scope = this;

                var url = Constants.WEBSERVICE.ENTITY_SERVICE_URL + Constants.WEBSERVICE.GET_ENTITY_LIST;

                HTTPService.executeHttpGetRequest(url).then(function (getEntityListResponse) {
                    var parsedGetEntityListResponse = {};
                    if (getEntityListResponse && getEntityListResponse.success && getEntityListResponse.responseData) {

                        if (getEntityListResponse.responseData.entityList) {
                            var parsedEntityList = [];
                            var entityList = getEntityListResponse.responseData.entityList;

                            for (var entityIndex = 0; entityIndex < entityList.length; entityIndex++) {
                                parsedEntityList.push(scope._retrieveEntityInstance(entityList[entityIndex]._id, entityList[entityIndex]));
                            }

                            parsedGetEntityListResponse['entityList'] = parsedEntityList;
                        } else {
                            parsedGetEntityListResponse['entityList'] = [];
                        }

                        if (getEntityListResponse.responseData.Token) {
                            parsedGetEntityListResponse['token'] = getEntityListResponse.responseData.Token;
                        } else {
                            parsedGetEntityListResponse['token'] = {};
                        }

                    } else {
                        parsedGetEntityListResponse['entityList'] = [];
                        parsedGetEntityListResponse['token'] = {};
                    }

                    parsedGetEntityListResponse['message'] = getEntityListResponse.message;

                    deferred.resolve(parsedGetEntityListResponse);
                });

                return deferred.promise;
            },

            addEntity: function (requestData) {
                var deferred = $q.defer();
                var scope = this;

                var url = Constants.WEBSERVICE.ENTITY_SERVICE_URL + Constants.WEBSERVICE.ADD_ENTITY;

                HTTPService.executeHttpPostRequest(url, requestData).then(function (addEntityResponse) {
                    var parsedAddEntityResponse = {};
                    if (addEntityResponse && addEntityResponse.success && addEntityResponse.responseData) {

                        var entityObj = addEntityResponse.responseData;
                        parsedAddEntityResponse['entityDetail'] = scope._retrieveEntityInstance(entityObj._id, entityObj);

                    } else {
                        parsedAddEntityResponse['entityDetail'] = {};
                    }

                    parsedAddEntityResponse['message'] = addEntityResponse.message;

                    deferred.resolve(parsedAddEntityResponse);
                });

                return deferred.promise;
            },

            updateEntity: function (requestData) {
                var deferred = $q.defer();
                var scope = this;

                var url = Constants.WEBSERVICE.ENTITY_SERVICE_URL + Constants.WEBSERVICE.UPDATE_ENTITY;

                HTTPService.executeHttpPostRequest(url, requestData).then(function (updateEntityResponse) {
                    var parsedUpdateEntityResponse = {};
                    if (updateEntityResponse && updateEntityResponse.success && updateEntityResponse.responseData) {

                        var entityObj = updateEntityResponse.responseData;
                        parsedUpdateEntityResponse['entityDetail'] = scope._retrieveEntityInstance(entityObj._id, entityObj);

                    } else {
                        parsedUpdateEntityResponse['entityDetail'] = {};
                    }

                    parsedUpdateEntityResponse['message'] = updateEntityResponse.message;

                    deferred.resolve(parsedUpdateEntityResponse);
                });

                return deferred.promise;
            }
        };
    }]);