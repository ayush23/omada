/**
 * Created by manishg on 07/09/17.
 */
angular.module('homer')
    .controller('PrivateDashboardController', ['$scope', '$rootScope', '$state', 'Constants', 'Utility', 'PrivateDashboardManager', '$mdDialog', 'EntityModel', 'SessionManager', function ($scope, $rootScope, $state, Constants, Utility, PrivateDashboardManager, $mdDialog, EntityModel, SessionManager) {
        var getProblemList = function (isToChangeSelectedProblem) {
            var requestData = {};

            PrivateDashboardManager.getEntityList(requestData).then(function getEntityListResponse(getEntityListResponse) {
                $scope.problemList = [];
                if (getEntityListResponse && getEntityListResponse.entityList && getEntityListResponse.token) {
                    $scope.token = getEntityListResponse.token;
                    $scope.problemList = getEntityListResponse.entityList;
                    $scope.problemList.sort(function (a, b) {
                        return b.addedOn - a.addedOn;
                    });
                    if ($scope.problemList.length > 0 && isToChangeSelectedProblem)
                        $scope.selectedProblem = $scope.problemList[0];

                } else {
                    Utility.displayToast(getEntityListResponse.message[0].msg || Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR, Constants.TOAST_TYPE.ERROR);
                }
            });
        };

        $scope.inItAddProblemFormParsley = function () {
            $('#addProblemForm').parsley();
        };

        $scope.inItGiveSolutionFormParsley = function () {
            $('#giveProblemForm').parsley();
        };

        $scope.showAddProblemDialog = function (ev) {
            $scope.addProblem = new EntityModel();
            $mdDialog.show({
                contentElement: '#addNewProblem',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            });
        };

        $scope.showGiveSolutionDialog = function (ev) {
            $scope.giveSolutionObject = {};
            $mdDialog.show({
                contentElement: '#solutionDialog',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false
            });
        };

        $scope.closeDialog = function () {
            $mdDialog.cancel();
        };

        $scope.submitProblem = function () {
            $scope.addProblem.entityData.postedBy = Utility.getMiniUserModel($scope.logedInUser);
            var requestData = {
                addedOn: new Date().getTime(),
                modifiedOn: new Date().getTime(),
                entityType: Constants.ENTITY_TYPE.PROBLEM,
                entityMetaData: JSON.stringify({}),
                entityData: JSON.stringify($scope.addProblem.entityData)
            };

            PrivateDashboardManager.addEntity(requestData).then(function addEntityResponse(addEntityResponse) {
                if (addEntityResponse && Object.keys(addEntityResponse.entityDetail).length > 0) {
                    Utility.displayToast(Constants.MESSAGE.SUCCESS_MESSAGE.SUBMIT_PROBLEM_SUCCESS, Constants.TOAST_TYPE.SUCCESS);
                    getProblemList(false);
                } else {
                    Utility.displayToast(addEntityResponse.message[0].msg || Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR, Constants.TOAST_TYPE.ERROR);
                }

                $scope.closeDialog();
            });
        };

        $scope.submitSolution = function (solution) {
            var solutionObj = {
                _id: Utility.generateRandomAlphaNumericString(11) + (new Date()).getTime(),
                description: solution.description,
                postedBy: Utility.getMiniUserModel($scope.logedInUser),
                votedBy: [],
                isSolution: 0,
                addedOn: (new Date()).getTime(),
                modifiedOn: (new Date()).getTime()
            };

            $scope.selectedProblem.entityData.solutions.push(solutionObj);

            var requestData = {
                entityID: $scope.selectedProblem._id,
                entityMetaData: JSON.stringify({}),
                entityData: JSON.stringify($scope.selectedProblem.entityData)
            };

            PrivateDashboardManager.updateEntity(requestData).then(function updateEntityResponse(updateEntityResponse) {
                if (updateEntityResponse && Object.keys(updateEntityResponse.entityDetail).length > 0) {
                    Utility.displayToast(Constants.MESSAGE.SUCCESS_MESSAGE.SUBMIT_SOLUTION_SUCCESS, Constants.TOAST_TYPE.SUCCESS);
                    getProblemList(false);
                } else {
                    Utility.displayToast(updateEntityResponse.message[0].msg || Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR, Constants.TOAST_TYPE.ERROR);
                }

                $scope.closeDialog();
            });
        };

        $scope.isSolutionSolvedProblem = function (solution) {
            return solution.isSolution;
        };

        $scope.isToShowSolvedProblem = function (problemObject, solution) {
            return problemObject.entityData.postedBy.userID === $scope.logedInUser.userID && !solution.isSolution && !isAnySolutionSolvedProblem(problemObject);
        };

        var isAnySolutionSolvedProblem = function (problemObject) {
            if (!problemObject || !problemObject.entityData.solutions)
                return true;
            var solutionList = problemObject.entityData.solutions;
            for (var index = 0; index < solutionList.length; index++) {
                if (solutionList[index].isSolution)
                    return true;
            }
            return false;
        };

        $scope.markProblemSolved = function (solution) {
            if ($scope.selectedProblem.entityData.postedBy.userID !== $scope.logedInUser.userID) {
                Utility.displayToast("You are not allowed to mark problem as solved" || Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR, Constants.TOAST_TYPE.ERROR);
            }

            var solutionList = $scope.selectedProblem.entityData.solutions;
            for (var solutionIndex = 0; solutionIndex < solutionList.length; solutionIndex++) {
                if (solutionList[solutionIndex]._id === solution._id) {
                    solutionList[solutionIndex].isSolution = 1;
                    break;
                }
            }

            $scope.selectedProblem.entityData.solutions = solutionList;

            var requestData = {
                isActive: 0,
                entityID: $scope.selectedProblem._id,
                entityMetaData: JSON.stringify({}),
                entityData: JSON.stringify($scope.selectedProblem.entityData)
            };

            PrivateDashboardManager.updateEntity(requestData).then(function updateEntityResponse(updateEntityResponse) {
                if (updateEntityResponse && Object.keys(updateEntityResponse.entityDetail).length > 0) {
                    Utility.displayToast(Constants.MESSAGE.SUCCESS_MESSAGE.SOLVED_PROBLEM_SUCCESS, Constants.TOAST_TYPE.SUCCESS);
                    getProblemList(false);
                } else {
                    Utility.displayToast(updateEntityResponse.message[0].msg || Constants.MESSAGE.ERROR_MESSAGE.WEBSERVICE_RESPONSE_ERROR, Constants.TOAST_TYPE.ERROR);
                }

                $mdDialog.cancel();
            });
        };

        $scope.getProblemDetail = function (problem) {
            $scope.selectedProblem = problem;
        };

        $scope.summernoteOptions = {
            height: 200,
            fontSizes: ['13', '14', '15', '16', '17', '18'],
            fontSize: '16',
            placeholder: 'Type here...',
            dialogsInBody: true,
            disableDragAndDrop: true,
            toolbar: [
                ['edit', ['undo', 'redo']],
                ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
                ['insert', ['link', 'hr']],
                ['view', ['fullscreen', 'codeview']]

            ]
        };

        var init = function () {
            $scope.logedInUser = SessionManager.getDataFromCookies(Constants.COOKIES.USER);
            $scope.token = {};
            $scope.selectedProblem = {};
            $scope.problemList = [];
            getProblemList(true);
        };

        if ($rootScope.validateSession()) {
            init();
        }
    }])
;
